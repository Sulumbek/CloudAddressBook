import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnect {

    private final static String HOST = "jdbc:postgresql://localhost:5432/Sulumbek";
    private final static String NAME = "postgres";
    private final static String PASSWORD = "8080";

    private Connection connection = null;

    public Connection getConnection(){
        return connection;
    }

    public void openConnect(){
        try {
            Class.forName("org.postgresql.Driver");
            connection =  DriverManager.getConnection(HOST, NAME, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void closeConnect(){
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
