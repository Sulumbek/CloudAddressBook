import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/signup")
public class ServletSignUp extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SessionsUsers sessionsUsers = new SessionsUsers();
        String email = req.getParameter("email").toLowerCase();
        boolean emailBool = CheckingField.acceptorEmail(email);
        String username = req.getParameter("username");
        boolean usernameBool = CheckingField.acceptorUsername(username);
        String [] password = req.getParameterValues("pass");
        boolean passwordBool = CheckingField.acceptorPassword(password);
        String name = req.getParameter("name");
        boolean nameBool = CheckingField.acceptorData(name);
        String surname = req.getParameter("surname");
        boolean surnameBool = CheckingField.acceptorData(surname);
        String paternalname = req.getParameter("paternalname");
        String country = req.getParameter("country");
        String city = req.getParameter("city");
        String telephone[] = req.getParameterValues("telephone");
        String facebook[] = req.getParameterValues("facebook");
        String mail[] = req.getParameterValues("mail");

        if(emailBool && usernameBool && passwordBool && nameBool&& surnameBool) {
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/accepted.html");
            int id = SeqModule.currentSequence("users_id_seq");
            User user = new User(username,email,password[0],false,0,false);
            Person person = new Person(id+1,name,surname,paternalname,new Location(country,city));

            for (String aTelephone : telephone) person.setContacts(new TelephoneContact(aTelephone));
            for (String aFacebook : facebook) person.setContacts(new FacebookContact(aFacebook));
            for (String aMail : mail) person.setContacts(new MailContact(aMail));

            sessionsUsers.addUsers(user,person);

            try {
                AcceptorMail.sendMail(email,"http://localhost:8080/CloudAddressBook/acceptor?id="+ (id+1));
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        else{
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/signup.html");
        }
    }

}
