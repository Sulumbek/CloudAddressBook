import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/main")
public class ServletMain extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session != null) {
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
        }else{
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("pass");
        String session = (String)req.getSession(true).getAttribute("email");
        int userId = CheckingSessions.getId(req);
        SessionsUsers sessionsUsers = new SessionsUsers(userId);
        boolean result  = sessionsUsers.verification(login,password);
        if(result){
            if(session==null) {
                req.getSession(true).setAttribute("email", login);
            }
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
        }
        else resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");
    }
}
