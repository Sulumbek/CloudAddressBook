import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

class CheckingSessions {
    // Нужно внедрить систему Сессий
    static int getId(HttpServletRequest req){
        String email = (String)req.getSession(true).getAttribute("email");
        try {
            return SessionsUsers.getIdByEmail(email);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
