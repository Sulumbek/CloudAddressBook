public class FacebookContact extends Contact{
    private String facebook;

    FacebookContact(String facebook) {
        this.facebook = facebook;
    }

    String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }
}
