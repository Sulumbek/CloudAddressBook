import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

class ConnectionPool {

    private List<PoolObject> poolObjects = new ArrayList<>();

    void put(Connection connection) {
        PoolObject poolObject = new PoolObject();
        poolObject.setConnection(connection);
        poolObjects.add(poolObject);
    }

    int invalidateBrokenConnections() throws SQLException {
        int count = 0;
        for (int i = 0; i<poolObjects.size();i++) {
            PoolObject poolObject = poolObjects.get(i);
            if (poolObject.getInUse() && poolObject.getConnection() != null) {
                if (poolObject.getConnection().isClosed() || !poolObject.getConnection().isValid(50)) {
                    poolObjects.remove(i);
                    count++;
                }else{
                    poolObjects.remove(i);
                    count++;
                }
            }
        }
        return count;
    }

    Connection getConnection() throws SQLException {
        for (PoolObject object: poolObjects) {
            if (!object.getInUse() && object.getConnection()!=null) {
                if (!object.getConnection().isClosed()) {
                    if (object.getConnection().isValid(50)) {
                        object.setInUse();
                        return object.getConnection();
                    }
                }
            }
        }
        return null;
    }

}
