public class User {

    private String username;
    private String email;
    private String password;
    private boolean acceptorEmail;
    private int count;
    private boolean premiumAccount;

    public User(String username, String email, String password, boolean acceptorEmail, int count, boolean premiumAccount) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.acceptorEmail = acceptorEmail;
        this.count = count;
        this.premiumAccount = premiumAccount;
    }

    public User() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isAcceptorEmail() {
        return acceptorEmail;
    }

    public void setAcceptorEmail(boolean acceptorEmail) {
        this.acceptorEmail = acceptorEmail;
    }

    public boolean isPremiumAccount() {
        return premiumAccount;
    }

    public void setPremiumAccount(boolean premiumAccount) {
        this.premiumAccount = premiumAccount;
    }
}
