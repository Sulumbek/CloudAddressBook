
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.Writer;

@WebServlet("/edit")
public class ServletEdit extends HttpServlet {
    private int userId;
    private User user;
    private Person person;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        userId = CheckingSessions.getId(req);
        SessionsUsers sessionsUsers = new SessionsUsers(userId);
        user = sessionsUsers.getUsersById();
        person = sessionsUsers.getUserDataById();
        Writer writer = resp.getWriter();
        writer.write(CodeHtml.codeMoreInfoPersons(userId,"edit",user,person));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");


        String username = req.getParameter("username");
        String [] password = req.getParameterValues("password");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String paternalname = req.getParameter("paternalname");
        String country = req.getParameter("country");
        String city = req.getParameter("city");
        System.out.println(country);
        System.out.println(city);
        SessionsUsers sessionsUsers = new SessionsUsers(userId);
        if(!username.equals(user.getUsername())) sessionsUsers.editUser(username,"username");
        if(CheckingField.acceptorPassword(password) && !password[0].equals(user.getPassword())) sessionsUsers.editUser(SecurityModule.getMD5(password[0]),"password");
        if(!name.equals(person.getName())) sessionsUsers.editPerson(name,"name");
        if(!surname.equals(person.getSurname())) sessionsUsers.editPerson(surname,"surname");
        if(!paternalname.equals(person.getPaternalname()))sessionsUsers.editPerson(paternalname,"paternalname");
        if(!country.equals(person.getLocation().getCountry()) || !city.equals(person.getLocation().getCity())){
            sessionsUsers.editLocation(country,city);
        }
        resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
    }
}
