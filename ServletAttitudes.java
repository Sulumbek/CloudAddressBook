import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet("/attitudes")
public class ServletAttitudes extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");


        int userId = CheckingSessions.getId(req);
        String colleagues = req.getParameter("colleagues");
        String friends = req.getParameter("friends");
        String relative = req.getParameter("relative");
        String students = req.getParameter("students");
        SessionsUsers sessionsUsers = new SessionsUsers(userId);

        Writer writer = resp.getWriter();
        if(colleagues != null && colleagues.equalsIgnoreCase("colleagues")){
            writer.write(sessionsUsers.getAttitudes("colleagues"));
        }
        else if(friends != null && friends.equalsIgnoreCase("friends")){
            writer.write(sessionsUsers.getAttitudes("friends"));
        }
        else if(relative != null && relative.equalsIgnoreCase("relative")){
            writer.write(sessionsUsers.getAttitudes("relative"));
        }
        else if(students != null && students.equalsIgnoreCase("students")){
            writer.write(sessionsUsers.getAttitudes("students"));
        }
        else resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        int userId = CheckingSessions.getId(req);
        String name = req.getParameter("param");
        int id = Integer.parseInt(req.getParameter("id"));

        SessionsUsers sessionsUsers = new SessionsUsers(userId);

        if(name.equalsIgnoreCase("friends"))sessionsUsers.addAttitudes(id,userId,name.toLowerCase());
        else if(name.equalsIgnoreCase("colleagues"))sessionsUsers.addAttitudes(id,userId,name.toLowerCase());
        else if(name.equalsIgnoreCase("relative"))sessionsUsers.addAttitudes(id,userId,name.toLowerCase());
        else if(name.equalsIgnoreCase("students"))sessionsUsers.addAttitudes(id,userId,name.toLowerCase());
        resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
    }
}
