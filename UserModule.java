import java.sql.*;
import java.util.ArrayList;

class UserModule {

    static User getUsersById(int userId) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();

        Statement statement  = connection.createStatement();
        User user = new User();
        ResultSet res = statement.executeQuery("Select * from users where id = "+userId);
        if(res.next()){
            user.setUsername(res.getString("username"));
            user.setPassword(res.getString("password"));
        }
        return user;
    }


    static void addUsersInDataBase(User user, Person person) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();

        PreparedStatement prepUser = connection.prepareStatement("INSERT into users(username, password, email, user_acceptor, persons_count, premium_account) values (?,?,?,?,?,?);");
        prepUser.setString(1,user.getUsername());
        prepUser.setString(2,SecurityModule.getMD5(user.getPassword()));
        prepUser.setString(3,user.getEmail());
        prepUser.setBoolean(4,user.isAcceptorEmail());
        prepUser.setInt(5,user.getCount());
        prepUser.setBoolean(6,user.isPremiumAccount());
        prepUser.executeUpdate();

        PreparedStatement prepUserData = connection.prepareStatement("INSERT INTO users_data(user_id, name, surname, paternalname) VALUES (?,?,?,?);");
        prepUserData.setInt(1,person.getId());
        prepUserData.setString(2,person.getName());
        prepUserData.setString(3,person.getSurname());
        prepUserData.setString(4,person.getPaternalname());
        prepUserData.executeUpdate();

        int id = SeqModule.currentSequence("users_data_id_seq");

        PreparedStatement prepLocation = connection.prepareStatement("INSERT  into users_location(users_data_id, countries, cities) VALUES (?,?,?);");
        prepLocation.setInt(1,id);
        prepLocation.setString(2,person.getLocation().getCountry());
        prepLocation.setString(3,person.getLocation().getCity());
        prepLocation.executeUpdate();

        ArrayList arrayList = person.getContacts();
        if(arrayList.size()>0) {

            PreparedStatement prepContact = connection.prepareStatement("INSERT  into  users_contacts(users_data_id) values (?);");
            prepContact.setInt(1, id);
            prepContact.executeUpdate();

            id = SeqModule.currentSequence("users_contacts_id_seq");
            for (Object anArrayList : arrayList) {
                if (anArrayList instanceof TelephoneContact) {
                    PreparedStatement prepTelephone = connection.prepareStatement("INSERT INTO users_telephones(users_contact_id, value) VALUES (?,?);");
                    prepTelephone.setInt(1, id);
                    prepTelephone.setString(2, ((TelephoneContact) anArrayList).getTel());
                    prepTelephone.executeUpdate();
                }
                if (anArrayList instanceof MailContact) {
                    PreparedStatement prepMail = connection.prepareStatement("INSERT INTO users_email(users_contact_id, value) VALUES (?,?);");
                    prepMail.setInt(1, id);
                    prepMail.setString(2, ((MailContact) anArrayList).getMail());
                    prepMail.executeUpdate();
                }
                if (anArrayList instanceof FacebookContact) {
                    PreparedStatement prepFacebook = connection.prepareStatement("INSERT INTO users_facebook(users_contact_id, value) VALUES (?,?);");
                    prepFacebook.setInt(1, id);
                    prepFacebook.setString(2, ((FacebookContact) anArrayList).getFacebook());
                    prepFacebook.executeUpdate();
                }
            }
        }

        myConnect.closeConnect();
    }
    static void addPremiumUserAccount(int userId, String name, int months, String key) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE users SET premium_account = true where id = "+userId+";");
        myConnect.closeConnect();
    }

    public static String getEmailById(int id){
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        String str = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select username,email from users where id ="+id);
            if (resultSet.next()){
                str = resultSet.getString("email");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        myConnect.closeConnect();
        return str;
    }

    static void usersAcceptorInDataBase(int id) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE users SET user_acceptor = true where id = "+id+";");
        myConnect.closeConnect();
    }

    static Person getUserData(int id) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statementPerson = connection.createStatement();
        ResultSet res = statementPerson.executeQuery("Select * from users_data where user_id ="+id+";");

        if (res.next()) {
            int idUserData = res.getInt("id");
            String name = res.getString("name");
            String surname = res.getString("surname");
            String paternalname = res.getString("paternalname");

            Statement statementLocation = connection.createStatement();
            ResultSet resultSet = statementLocation.executeQuery("Select * from users_location where users_data_id ="+idUserData+";");
            Location location = null;
            if (resultSet.next()) {
                location = new Location(resultSet.getString("countries"), resultSet.getString("cities"));
            }
            Person person = new Person(id, name, surname, paternalname, location);
            Statement statementContact = connection.createStatement();
            ResultSet resCon = statementContact.executeQuery("SELECT * from users_contacts where users_data_id="+ idUserData + ";");
            if (resCon.next()) {
                int idContact = resCon.getInt("id");
                Statement statementTel = connection.createStatement();
                ResultSet resTel = statementTel.executeQuery("Select * from users_telephones where users_contact_id ="+ idContact);
                while (resTel.next()) person.setContacts(new TelephoneContact(resTel.getString("value")));

                Statement statementMail = connection.createStatement();
                ResultSet resMail = statementMail.executeQuery("Select * from users_email where users_contact_id ="+ idContact);
                while (resMail.next()) person.setContacts(new MailContact(resMail.getString("value")));

                Statement statementFace = connection.createStatement();
                ResultSet resFace = statementFace.executeQuery("Select * from users_facebook where users_contact_id ="+ idContact);
                while (resFace.next()) person.setContacts(new FacebookContact(resFace.getString("value")));
            }
            myConnect.closeConnect();
            return person;
        }
        return null;
    }

    static void edit(int userId,String field, String key) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE users SET "+key+" = '"+field+"' where id = "+userId+";");
        myConnect.closeConnect();
    }
}
