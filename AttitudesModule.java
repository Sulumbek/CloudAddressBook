import java.sql.*;
import java.util.ArrayList;

public class AttitudesModule {

    static void setAttitudesInDataBase(int personId, int userId, String param) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT into "+param+"(users_id,persons_id) values (?,?);");
        preparedStatement.setInt(1,userId);
        preparedStatement.setInt(2,personId);
        preparedStatement.executeUpdate();
        myConnect.closeConnect();
    }

    static ArrayList<Person> getAttitudesOfDataBase(int userId, String param) throws SQLException {
        ArrayList<Person> arrayList = new ArrayList<>();
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery("Select persons_id from "+param+" where users_id="+userId+";");
        while (res.next()){
            arrayList.add(PersonsModule.getPersonsByIdOfDataBase(res.getInt("persons_id")));
        }
        myConnect.closeConnect();
        return arrayList;
    }
}
