import java.io.IOException;
import java.sql.*;

public class ConnectionPoolManager extends Thread {

    private ConnectionPool connectionPool;
    private int count;

    ConnectionPoolManager(ConnectionPool pool,int count) throws IOException {
        this.connectionPool = pool;
        this.count = count;
    }

    @Override
    public void run() {
        for (int i=0; i<count; i++) {
            ConnectionThread connectionThread = new ConnectionThread(connectionPool);
            connectionThread.start();
        }
        try {
            checkPool();
        } catch (InterruptedException | SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void checkPool() throws InterruptedException, SQLException, IOException {
        while (true) {
            Thread.sleep(60000);
            int countConnection = connectionPool.invalidateBrokenConnections();
            System.out.println(countConnection);
            for (int i = 0; i < countConnection; i++) {
                ConnectionThread connectionThread = new ConnectionThread(connectionPool);
                connectionThread.start();
            }
        }
    }

}
