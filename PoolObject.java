import java.sql.Connection;

class PoolObject {

    private Boolean isInUse = false;
    private Connection connection;

    Boolean getInUse() {
        return isInUse;
    }

    void setInUse() {
        isInUse = true;
    }

    Connection getConnection() {
        return connection;
    }

    void setConnection(Connection connection) {
        this.connection = connection;
    }
}
