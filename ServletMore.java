import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet("/more")

public class ServletMore extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        int id = Integer.parseInt(req.getParameter("id"));
        SessionsUsers sessionsUsers = new SessionsUsers();
        Person person = sessionsUsers.getPersonById(id);
        String s = CodeHtml.codeMoreInfoPersons(id,"more",null,person);
        Writer writer = resp.getWriter();
        writer.write(s);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        int userId = CheckingSessions.getId(req);
        int personId = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String paternalname = req.getParameter("paternalname");
        String country = req.getParameter("country");
        String city = req.getParameter("city");
        String [] telephone = req.getParameterValues("telephone");
        String [] facebook = req.getParameterValues("facebook");
        String [] mail = req.getParameterValues("mail");
        boolean nameBool = CheckingField.acceptorData(name);
        boolean surnameBool = CheckingField.acceptorData(surname);
        if(nameBool && surnameBool){
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
            Person person = new Person(name,surname,paternalname,new Location(country,city));
            for (String aTelephone : telephone) person.setContacts(new TelephoneContact(aTelephone));
            for (String aFacebook : facebook) person.setContacts(new FacebookContact(aFacebook));
            for (String aMail : mail) person.setContacts(new MailContact(aMail));
            SessionsUsers sessionsUsers = new SessionsUsers(userId);
            sessionsUsers.deletePerson(personId);
            sessionsUsers.addPersons(person);
        }else{
            doGet(req,resp);
        }

    }
}
