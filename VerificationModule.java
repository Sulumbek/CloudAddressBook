import java.sql.*;

public class VerificationModule {

    private int userId;

    void setUserId(int id){
        this.userId = id;
    }
    int getUserId(){
        return userId;
    }

    static boolean checkEmailAndLogin(String str) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT username,email from users;");
        while (result.next()){
            if(str.equalsIgnoreCase(result.getString("username"))){
                return false;
            }
            else if(str.equalsIgnoreCase(result.getString("email"))){
                return false;
            }
        }
        myConnect.closeConnect();
        return true;
    }

    static boolean verificationLoginAndEmail(String username, String password) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * from users");
        while (result.next()) {
            String login = result.getString("username");
            String pass = result.getString("password");
            String email = result.getString("email");
            boolean acceptor = result.getBoolean("user_acceptor");
            if ((login.equals(username) || email.equals(username)) && SecurityModule.getMD5(password).equals(pass) && acceptor) {
                return true;
            }
        }
        myConnect.closeConnect();
        return false;
    }
}
