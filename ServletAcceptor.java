import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/acceptor")
public class ServletAcceptor extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        SessionsUsers sessionsUsers = new SessionsUsers(id);
        sessionsUsers.usersAcceptor(id);

        resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){

    }
}
