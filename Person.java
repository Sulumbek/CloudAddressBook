import java.util.ArrayList;

public class Person {
    private int id;
    private String name;
    private String surname;
    private String paternalname;
    private ArrayList<Contact>contacts = new ArrayList<Contact>();
    private Location location;

    Person(int id ,String name, String surname, String paternalname,Location location) {
        this.name = name;
        this.surname = surname;
        this.paternalname = paternalname;
        this.id = id;
        this.location = location;
    }

    Person(String name, String surname, String paternalname,Location location){
        this.name = name;
        this.surname = surname;
        this.paternalname = paternalname;
        this.location = location;
    }

    Person(int id,String name, String surname, String paternalname){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.paternalname = paternalname;
    }

    Location getLocation() {
        if(location == null){
            return  new Location();
        }
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPaternalname() {
        return paternalname;
    }

    public void setPaternalname(String paternalname) {
        this.paternalname = paternalname;
    }

    ArrayList<Contact> getContacts() {
        return contacts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    void setContacts(Contact contacts){
        this.contacts.add(contacts);
    }
}
