import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/premium")

public class ServletPremium extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        // Нужно внедрить систему оплаты...
        int userId = CheckingSessions.getId(req);
        SessionsUsers sessionsUsers = new SessionsUsers(userId);
        String name = req.getParameter("param1");
        int months = Integer.parseInt(req.getParameter("param2"));
        String key = req.getParameter("key");
        boolean keyBool = CheckingField.acceptorKey(key);
        if(keyBool){
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
            sessionsUsers.addPremium(name,months,key);
        }else{
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/premium.html");
        }
    }
}
