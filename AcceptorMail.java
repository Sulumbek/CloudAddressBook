import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class AcceptorMail {

    private static final String USER = "cloudaddressbook94@gmail.com";
    private static final String PASSWORD = "Sulumbek94S";

    static void sendMail(String mail,String link) throws MessagingException {
        final Properties properties = new Properties();
        properties.put("mail.transport.protocol","smtps");
        properties.put("mail.smtp.auth","true");

        Session session = Session.getDefaultInstance(properties);

        MimeMessage mess = new MimeMessage(session);
        mess.setFrom(new InternetAddress(USER));
        mess.setSubject("Acceptor Mail Cloud Book Address");
        mess.setText("If you have registered on the Cloud address Book website, activate the link:\n"+link);
        mess.addRecipient(Message.RecipientType.TO, new InternetAddress(mail));
        mess.setSentDate(new Date());

        Transport transport = session.getTransport();
        transport.connect("smtp.gmail.com",465, USER,PASSWORD);
        transport.sendMessage(mess,mess.getAllRecipients());
        transport.close();
    }
}
