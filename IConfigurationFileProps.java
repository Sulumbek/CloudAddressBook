public class IConfigurationFileProps {

    private static String host;
    private static String nameDB;
    private static String password;

    static String getHost() {
        return host;
    }

    static void setHost(String host) {
        IConfigurationFileProps.host = host;
    }

    static String getNameDB() {
        return nameDB;
    }

    static void setNameDB(String nameDB) {
        IConfigurationFileProps.nameDB = nameDB;
    }

    static String getPassword() {
        return password;
    }

    static void setPassword(String password) {
        IConfigurationFileProps.password = password;
    }
}
