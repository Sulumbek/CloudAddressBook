import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataSource {

    private int maxConnections = 40;
    private int minConnections = 20;

    private ConnectionPool pool;
    private static DataSource dataSource;

    private DataSource() throws IOException, InterruptedException {
        readFile();
        pool = new ConnectionPool();
        ConnectionPoolManager manager = new ConnectionPoolManager(pool,maxConnections);
        manager.start();
    }
    public ConnectionPool getPool(){
        return pool;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getMinConnections() {
        return minConnections;
    }

    public void setMinConnections(int minConnections) {
        this.minConnections = minConnections;
    }

    static void readFile(){
        Properties properties = new Properties();
        try {
            FileInputStream file = new FileInputStream("config.properties");
            properties.load(file);
            String host = properties.getProperty("host");
            String name = properties.getProperty("name");
            String pass = properties.getProperty("pass");
            String schema = properties.getProperty("schema");
            int port = Integer.parseInt(properties.getProperty("port"));
            String db = properties.getProperty("db");

            IConfigurationFileProps.setHost("jdbc:"+db+"://"+host+":"+port+"/"+schema);
            IConfigurationFileProps.setNameDB(name);
            IConfigurationFileProps.setPassword(pass);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static DataSource newInstance() throws IOException, InterruptedException {
        if (dataSource==null) {
            dataSource = new DataSource();
            Thread.sleep(10000);
        }else {
            return dataSource;
        }
        return dataSource;
    }

    public Connection getConnection() throws SQLException {
        return pool.getConnection();
    }
}
