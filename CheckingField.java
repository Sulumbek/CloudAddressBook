import java.sql.SQLException;

public class CheckingField {

    static boolean acceptorEmail(String email) {
        boolean accepted = false;
        if(email.length()>10 && (email.endsWith("@gmail.com") || email.endsWith("@mail.ru"))){
            try {
                accepted = VerificationModule.checkEmailAndLogin(email.toLowerCase());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return accepted;
    }
    static boolean acceptorUsername(String username) {
        boolean accepted = false;
        if(username.length()>3){
            try {
                accepted = VerificationModule.checkEmailAndLogin(username);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return accepted;
    }
    static boolean acceptorKey(String key) {
        return key.length()>11;
    }
    static boolean acceptorPassword(String [] password) {
        return password[0].equals(password[1]) && password[0].length() > 5;
    }
    static boolean acceptorData(String name) {
        return name.length()>1;
    }
    public static boolean acceptorCountry(String country) {
        return country.length()>3;
    }
    public static boolean acceptorCity(String city) {
        return city.length()>3;
    }

}
