import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionThread extends Thread{

    private ConnectionPool pool;
    ConnectionThread(ConnectionPool connectionPool){
        this.pool = connectionPool;
    }
    @Override
    public void run() {
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection =  DriverManager.getConnection(IConfigurationFileProps.getHost(),IConfigurationFileProps.getNameDB(),IConfigurationFileProps.getPassword());
            pool.put(connection);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
