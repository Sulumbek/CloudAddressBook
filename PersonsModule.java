import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class PersonsModule {

    static int currentCountPersons(int userId) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT persons_count from users where id = "+userId+";");
        if(resultSet.next()){
            myConnect.closeConnect();
            return resultSet.getInt("persons_count");
        }
        myConnect.closeConnect();
        return 0;
    }
    static ArrayList<Person> searchPersons(int userId,String qName,String qSurname) throws SQLException, IOException, InterruptedException {
//        DataSource ds = DataSource.newInstance();
//        Connection connection = ds.getConnection();

        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery("SELECT * FROM persons where users_id = "+userId+";");
        ArrayList<Person> list = new ArrayList<>();
        while (res.next()) {
            int id = res.getInt("id");
            String name = res.getString("name");
            String surname = res.getString("surname");
            String paternalname = res.getString("paternalname");
            if ((qName.length()>0 && name.contains(qName)) || (qSurname.length()>0 && surname.contains(qSurname))) {
                list.add(new Person(id,name,surname,paternalname));
            }
        }
        myConnect.closeConnect();
        return list;
    }

    static Person getPersonsByIdOfDataBase(int id) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statementPerson = connection.createStatement();
        ResultSet res = statementPerson.executeQuery("Select * from persons where id ="+id+";");
        if(res.next()){
            String name = res.getString("name");
            String surname = res.getString("surname");
            String paternalname = res.getString("paternalname");

            Statement statementLocation = connection.createStatement();
            ResultSet resultSet = statementLocation.executeQuery("Select * from persons_location where persons_id="+id+";");
            Location location = null;
            if(resultSet.next()){
                location = new Location(resultSet.getString("countries"),resultSet.getString("cities"));
            }
            Person person = new Person(id,name,surname,paternalname,location);
            Statement statementContact = connection.createStatement();
            ResultSet resCon = statementContact.executeQuery("SELECT * from persons_contacts where persons_id="+id+";");
            if (resCon.next()) {
                int idContact = resCon.getInt("id");

                Statement statementTel = connection.createStatement();
                ResultSet resTel = statementTel.executeQuery("Select * from persons_telephones where persons_contact_id ="+idContact);
                while (resTel.next()) person.setContacts(new TelephoneContact(resTel.getString("value")));

                Statement statementMail = connection.createStatement();
                ResultSet resMail = statementMail.executeQuery("Select * from persons_email where persons_contact_id ="+idContact);
                while (resMail.next()) person.setContacts(new MailContact(resMail.getString("value")));

                Statement statementFace = connection.createStatement();
                ResultSet resFace = statementFace.executeQuery("Select * from persons_facebook where persons_contact_id ="+idContact);
                while (resFace.next()) person.setContacts(new FacebookContact(resFace.getString("value")));
            }
            myConnect.closeConnect();

            return person;
        }
        return null;
    }

    static void addPersonsInDataBase(int userId, Person person) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();

        PreparedStatement prepPersons = connection.prepareStatement("INSERT INTO persons(name, surname, paternalname, users_id, person_count) VALUES (?,?,?,?,?)");
        prepPersons.setString(1,person.getName());
        prepPersons.setString(2,person.getSurname());
        prepPersons.setString(3,person.getPaternalname());
        prepPersons.setInt(4,userId);
        prepPersons.setInt(5,0);
        prepPersons.executeUpdate();

        int id = SeqModule.currentSequence("persons_id_seq");

        PreparedStatement prepLocation = connection.prepareStatement("INSERT INTO persons_location(persons_id, address_type, countries, cities) VALUES (?,?,?,?); ");
        prepLocation.setInt(1,id);
        prepLocation.setString(2,null);
        prepLocation.setString(3,person.getLocation().getCountry());
        prepLocation.setString(4,person.getLocation().getCity());
        prepLocation.executeUpdate();

        ArrayList arrayList = person.getContacts();
        if(arrayList.size()>0){

            PreparedStatement prepContact = connection.prepareStatement("INSERT  into  persons_contacts(persons_id) values (?);");
            prepContact.setInt(1,id);
            prepContact.executeUpdate();
            int idContact = SeqModule.currentSequence("persons_contacts_id_seq");

            for (Object anArrayList : arrayList) {
                if (anArrayList instanceof TelephoneContact) {
                    PreparedStatement prepTelephone = connection.prepareStatement("INSERT INTO persons_telephones(persons_contact_id, value) VALUES (?,?);");
                    prepTelephone.setInt(1, idContact);
                    prepTelephone.setString(2, ((TelephoneContact) anArrayList).getTel());
                    prepTelephone.executeUpdate();
                }
                if (anArrayList instanceof MailContact) {
                    PreparedStatement prepMail = connection.prepareStatement("INSERT INTO persons_email(persons_contact_id, value) VALUES (?,?);");
                    prepMail.setInt(1, idContact);
                    prepMail.setString(2, ((MailContact) anArrayList).getMail());
                    prepMail.executeUpdate();
                }
                if (anArrayList instanceof FacebookContact) {
                    PreparedStatement prepFacebook = connection.prepareStatement("INSERT INTO persons_facebook(persons_contact_id, value) VALUES (?,?);");
                    prepFacebook.setInt(1, idContact);
                    prepFacebook.setString(2, ((FacebookContact) anArrayList).getFacebook());
                    prepFacebook.executeUpdate();
                }
            }
        }
        myConnect.closeConnect();
    }

    static void deletePersonOfDataBase(int idPerson) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        int idContact = 0;
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT id from persons_contacts where persons_id ="+idPerson+";");
        if (resultSet.next()) idContact = resultSet.getInt("id");
        if(idContact!=0) {
            Statement deleteMail = connection.createStatement();
            deleteMail.executeUpdate("DELETE FROM persons_email where persons_contact_id=" + idContact + ";");

            Statement deleteFacebook = connection.createStatement();
            deleteFacebook.executeUpdate("DELETE FROM persons_facebook where persons_contact_id=" + idContact + ";");

            Statement deleteTel = connection.createStatement();
            deleteTel.executeUpdate("DELETE FROM persons_telephones where persons_contact_id=" + idContact + ";");

            Statement deleteContact = connection.createStatement();
            deleteContact.executeUpdate("DELETE FROM persons_contacts where persons_id =" + idPerson + ";");
        }
        Statement deleteLocation = connection.createStatement();
        deleteLocation.executeUpdate("DELETE FROM persons_location where persons_id ="+idPerson+";");

        Statement deleteFriends = connection.createStatement();
        deleteFriends.executeUpdate("DELETE FROM friends where persons_id ="+idPerson+";");

        Statement deleteColle = connection.createStatement();
        deleteColle.executeUpdate("DELETE FROM colleagues where persons_id ="+idPerson+";");

        Statement deleteStudents = connection.createStatement();
        deleteStudents.executeUpdate("DELETE FROM students where persons_id ="+idPerson+";");

        Statement deleteRelative = connection.createStatement();
        deleteRelative.executeUpdate("DELETE FROM relative where persons_id ="+idPerson+";");

        Statement deletePerson = connection.createStatement();
        deletePerson.executeUpdate("DELETE FROM persons WHERE id ="+idPerson);

        myConnect.closeConnect();
    }

    static void updateCountPersonsInDataBase(int countPerson, int userId) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE users SET persons_count = "+countPerson+" where id = "+userId+";");
        myConnect.closeConnect();
    }

    static void editPersonInDataBase(int userId,String field, String key) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate("UPDATE users_data SET "+key+" = '"+field+"' where user_id = "+userId+";");
        myConnect.closeConnect();
    }

    static void editLocationInDataBase(int userId,String country, String city) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement tempStatement = connection.createStatement();
        ResultSet resultSet = tempStatement.executeQuery("Select * from users_location where users_data_id ="+userId);
        if(resultSet.next()){
            Statement statement = connection.createStatement();
            statement.executeUpdate("UPDATE users_location SET countries = '" + country + "' WHERE users_data_id ="+userId);
            statement.executeUpdate("UPDATE users_location SET cities = '" + city + "' WHERE users_data_id ="+userId );
        }else {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into users_location(users_data_id, countries, cities) values (?,?,?)");
            preparedStatement.setInt(1,userId);
            preparedStatement.setString(2,country);
            preparedStatement.setString(3,city);
            preparedStatement.executeUpdate();
        }
        myConnect.closeConnect();
    }
}
