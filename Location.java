public class Location {
    private String country;
    private String city;

    Location(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public Location() {

    }

    public String getCountry() {
        if(country==null) return "";
        return country;
    }

    public String getCity() {
        if(city==null) return "";
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
