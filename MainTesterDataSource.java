import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainTesterDataSource {

    public static void main(String[] args) throws IOException {
        try {
            DataSource ds = DataSource.newInstance();
            Connection connection = ds.getConnection();
            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery("Select * from users;");
            while (res.next()){
                System.out.println(res.getString(2)+" "+ res.getString(3));
            }
        } catch (InterruptedException | SQLException e) {
            e.printStackTrace();
        }
    }
}
