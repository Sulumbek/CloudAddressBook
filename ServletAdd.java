import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add")
public class ServletAdd extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");


        int userId = CheckingSessions.getId(req);

        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String paternalname = req.getParameter("paternalname");
        String country = req.getParameter("country");
        String city = req.getParameter("city");
        String [] telephone = req.getParameterValues("telephone");
        String [] facebook = req.getParameterValues("facebook");
        String [] mail = req.getParameterValues("mail");

        boolean nameBool = CheckingField.acceptorData(name);
        boolean surnameBool = CheckingField.acceptorData(surname);

        SessionsUsers sessionsUsers = new SessionsUsers(userId);
        int countPerson = sessionsUsers.getCurrentCountPersons();
        if(nameBool && surnameBool && countPerson < 15){
                resp.sendRedirect("http://localhost:8080/CloudAddressBook/addnew.html");
                Location location = new Location(country,city);
                Person person = new Person(name,surname,paternalname,location);
                for (String aTelephone : telephone) person.setContacts(new TelephoneContact(aTelephone));
                for (String aFacebook : facebook) person.setContacts(new FacebookContact(aFacebook));
                for (String aMail : mail) person.setContacts(new MailContact(aMail));
                sessionsUsers.addPersons(person);
                sessionsUsers.updateCountPersons(countPerson+1);
        }
        else{
            resp.sendRedirect("http://localhost:8080/CloudAddressBook/addnew2.html");
        }
    }
}
