public class TelephoneContact extends Contact{
    private String tel;

    public TelephoneContact(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
