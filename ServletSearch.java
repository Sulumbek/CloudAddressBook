import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet("/search")
public class ServletSearch extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        int userId = CheckingSessions.getId(req);
        String qName = req.getParameter("name");
        String qSurname = req.getParameter("surname");
        Writer writer = resp.getWriter();
        if(qName.length()>0 || qSurname.length()>0) {
            SessionsUsers sessionsUsers = new SessionsUsers(userId);
            writer.write(CodeHtml.codeViewPersonsList(sessionsUsers.findPersons(qName, qSurname)));
        }else resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
    }

}