import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete")
public class ServletDelete extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

        int userId = CheckingSessions.getId(req);
        int personId = Integer.parseInt(req.getParameter("id"));
        SessionsUsers sessionsUsers = new SessionsUsers(userId);
        int countPerson = sessionsUsers.getCurrentCountPersons();
        sessionsUsers.deletePerson(personId);
        sessionsUsers.updateCountPersons(countPerson-1);
        resp.sendRedirect("http://localhost:8080/CloudAddressBook/search.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String session = (String)req.getSession(true).getAttribute("email");
        if(session == null) resp.sendRedirect("http://localhost:8080/CloudAddressBook/main.html");

    }
}
