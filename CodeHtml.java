import java.util.ArrayList;

class CodeHtml {

    static  String codeMoreInfoPersons(int id,String action,User user ,Person person){

        String s = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<title>Edit</title>\n" +
                "\t<style>\n" +
                "\t\tbody{\n" +
                "\t\t\tbackground-image: url(43.png);\n" +
                "\t\t}\n" +
                "\t\t.art{\n" +
                "\t\t\tpadding-top: 20px;\n" +
                "\t\t\tbackground-color: #224b79;\n" +
                "\t\t\theight: 1100px;\n" +
                "\t\t\twidth: 600px;\n" +
                "\t\t\tborder-radius: 100px;\n" +
                "\t\t\tborder-color: white;\n" +
                "\t\t\tborder: white 3px solid;\n" +
                "\t\t}\n" +
                "\t\t.header{\n" +
                "\t\t\tmargin-bottom: 20px;\n" +
                "\t\t}\n" +
                "\t\t.p{\n" +
                "\t\t\tmargin-left: 43px;\n" +
                "\t\t}\n" +
                "\t\t.login{\n" +
                "\t\t\tfont-size: 17px;\n" +
                "\t\t\t\n" +
                "\t\t}\n" +
                "\t\t.reg1{\n" +
                "\t\t\theight: 50px;\n" +
                "\t\t\twidth: 130px;\n" +
                "\t\t\tbackground-color: #7B68EE;\n" +
                "\t\t\tborder-radius: 10px;\n" +
                "\t\t\tfont-size: 25px;\n" +
                "\t\t\tcolor: white;\n" +
                "\t\t\tborder-color: white;\n" +
                "\t\t}\n" +
                "\t\t.regis{\n" +
                "\t\t\theight: 100px;\n" +
                "\t\t\twidth: 100px;\n" +
                "\t\t\tfloat: left;\n" +
                "\t\t\tpadding-top: 30px;\n" +
                "\t\t\tpadding-left: 150px;\n" +
                "\t\t}\n" +
                "\t\t.header{\n" +
                "\t\t\theight: 100px;\n" +
                "\t\t\twidth: 1265px;\n" +
                "\t\t\tbackground-color: #483D8B;\n" +
                "\t\t}\n" +
                "\t\t.title{\n" +
                "\t\t\tpadding-left: 200px;\n" +
                "\t\t\tcolor: #7B68EE;\n" +
                "\t\t\tfont-size: 60px; \n" +
                "\t\t\theight: 70px;\n" +
                "\t\t\twidth: 600px;\n" +
                "\t\t\tfloat:left;\n" +
                "\t\t\tpadding-top: 10px;\n" +
                "\t\t}\n" +
                "\t\t.logo{\n" +
                "\t\t\theight: 100px;\n" +
                "\t\t\twidth: 100px;\n" +
                "\t\t\tpadding-top: 5px;\n" +
                "\t\t\tpadding-left: 5px;\n" +
                "\t\t\tfloat: left;\n" +
                "\t\t}\n" +
                "\n" +
                "\n" +
                "\t</style>\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t<div class=\"header\">\n" +
                "\t\t\t<div class=\"logo\">\n" +
                "\t\t\t\t<a href=\"http://localhost:8080/CloudAddressBook/search.html\"><img src=\"logo.png\" alt=\"Logo\" height=\"90px\"></a>\n" +
                "\t\t\t</div>\n" +
                "\t\t\t<div class=\"title\">\n" +
                "\t\t\t\t<center>\n" +
                "\t\t\t\t\tEDIT\n" +
                "\t\t\t\t</center>\n" +
                "\t\t\t</div>\n" +
                "\t\t</div>\n" +
                "\t\t<center>\n" +
                "\t\t<div class=\"art\">\n" +
                "\t\t\t<div class=\"login\">\n" +
                "\t\t\t\t<form id=\"block\" action=\""+action+"?id="+id+"\" method=\"POST\">";

                if(user != null) {
                            s+="\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">* Username: </label>\n" +
                            "\t\t\t\t\t<input value=" + user.getUsername() + " style=\"margin-left:60px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"username\"><br><br>\n" +
                            "\t\t\t\t\t<label  style=\"font-size: 20px; color:white;\">* Password: </label>\n" +
                            "\t\t\t\t\t<input value=" + user.getPassword() + " style=\"margin-left:60px; font-size: 20px;border-radius: 10px;\" type=\"password\" name=\"password\"><br><br>\n" +
                            "\t\t\t\t\t<label  style=\"font-size: 20px; color:white;\">* Password: </label>\n" +
                            "\t\t\t\t\t<input value=" + user.getPassword() + " style=\"margin-left:60px; font-size: 20px;border-radius: 10px;\" type=\"password\" name=\"password\"><br><br>\n";
                }

                s+="\n" +
                "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">* Name: </label>\n" +
                "\t\t\t\t\t<input value="+person.getName()+" style=\"margin-left:90px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"name\"><br><br>\n" +
                "\n" +
                "\t\t\t\t\t<label  style=\"font-size: 20px; color:white;\">* Surname: </label>\n" +
                "\t\t\t\t\t<input value="+person.getSurname()+" style=\"margin-left:69px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"surname\"><br><br>\n" +
                "\n" +
                "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Fathername: </label>\n" +
                "\t\t\t\t\t<input style=\"margin-left:50px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"paternalname\" value="+person.getPaternalname()+" ><br><br>\n" +
                "\n" +
                "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Country: </label>\n" +
                "\t\t\t\t\t<input style=\"margin-left:80px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"country\"  value="+person.getLocation().getCountry()+" ><br><br>\n" +
                "\n" +
                "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; City: </label>\n" +
                "\t\t\t\t\t<input style=\"margin-left:110px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"city\"  value="+person.getLocation().getCity()+"><br><br>\n";
                for (Object anArrayList : person.getContacts()) {
                    if (anArrayList instanceof TelephoneContact) {
                        s += "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Telephone: </label>\n" +
                                "\t\t\t\t\t<input style=\"margin-left:60px; font-size: 20px;border-radius: 10px;\" type=\"tel\" name=\"telephone\"  value=" + ((TelephoneContact) anArrayList).getTel() + " ><br><br>\n";
                    }
                    if (anArrayList instanceof MailContact) {
                        s += "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Mail: </label>\n" +
                                "\t\t\t\t\t<input style=\"margin-left:65px; margin-left:105px; font-size: 20px;border-radius: 10px;\" type=\"email\" name=\"mail\" value=" + ((MailContact) anArrayList).getMail() + " ><br><br>\n";
                    }
                    if (anArrayList instanceof FacebookContact) {
                        s += "\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Facebook: </label>\n" +
                                "\t\t\t\t\t<input style=\"margin-left:65px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"facebook\"  value=" + ((FacebookContact) anArrayList).getFacebook() + "><br><br>\n";
                    }
                }


        s+="\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Telephone: </label>\n" +
                "\t\t\t\t\t<input style=\"margin-left:60px; font-size: 20px;border-radius: 10px;\" type=\"tel\" name=\"telephone\"><br><br>";
        s+="\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Facebook: </label>\n" +
                "\t\t\t\t\t<input style=\"margin-left:65px; font-size: 20px;border-radius: 10px;\" type=\"text\" name=\"facebook\"><br><br>";
        s+="\t\t\t\t\t<label style=\"font-size: 20px; color:white;\">&nbsp; Mail: </label>\n" +
                "\t\t\t\t\t<input style=\"margin-left:105px; font-size: 20px;border-radius: 10px;\" type=\"email\" name=\"mail\"><br><br>";

        s+= "\t\t\t\t\t<input type=\"submit\" class=\"reg1\" value=\"EDIT\"><br><br>\n" +
                "\n" +
                "\t\t\t\t</form>\n" +
                "        \t</div>\n" +
                "        </div>\n" +
                "\t\t</center>\n" +
                "</body>\n" +
                "</html>";

        return s;
    }

    static String codeViewPersonsList(ArrayList arrayList){
        String str = "<html>\n" +
                "<head>\n" +
                "        <title>Search</title>\n" +
                "        <link rel=\"stylesheet\" type=\"text/css\" href=\"search.css\">\n" +
                "        <style type=\"text/css\">\n" +
                "\n" +
                "body{\n" +
                "\tbackground-image: url(43.png);\n" +
                "}\n" +
                ".art{\n" +
                "\tpadding-top: 50px;\n" +
                "\tbackground-color: #224b79;\n" +
                "\theight: 1000px;\n" +
                "\twidth: 1000px;\n" +
                "\tborder-radius: 100px;\n" +
                "\tborder-color: white;\n" +
                "\tborder: white 3px solid;\n" +
                "}\n" +
                ".login{\n" +
                "\tfont-size: 17px;\n" +
                "\t\n" +
                "}\n" +
                ".reg1{\n" +
                "\theight: 60px;\n" +
                "\twidth: 200px;\n" +
                "\tbackground-color: #7B68EE;\n" +
                "\tborder-radius: 10px;\n" +
                "\tfont-size: 25px;\n" +
                "\tcolor: white;\n" +
                "\tborder-color: white;\n" +
                "}\n" +
                ".regis{\n" +
                "\theight: 100px;\n" +
                "\twidth: 100px;\n" +
                "\tfloat: left;\n" +
                "\tpadding-top: 30px;\n" +
                "\tpadding-left: 150px;\n" +
                "}\n" +
                ".header{\n" +
                "\theight: 100px;\n" +
                "\twidth: 1265px;\n" +
                "\tbackground-color: #483D8B;\n" +
                "}\n" +
                ".title{\n" +
                "\tpadding-left: 400px;\n" +
                "\tcolor: #7B68EE;\n" +
                "\tfont-size: 60px; \n" +
                "\theight: 70px;\n" +
                "\twidth: 200px;\n" +
                "\tfloat:left;\n" +
                "\tpadding-top: 10px;\n" +
                "}\n" +
                ".logo {\n" +
                "        height: 100px;\n" +
                "        width: 100px;\n" +
                "        padding-top: 5px;\n" +
                "        padding-left: 5px;\n" +
                "        float: left;\n" +
                "}</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "        <header>\n" +
                "                <div class=\"header\">\n" +
                "                        <div class=\"logo\">\n" +
                "                                <a href=\"http://localhost:8080/CloudAddressBook/search.html\"><img src=\"logo.png\" alt=\"Logo\" height=\"90px\"></a>\n" +
                "                        </div>\n" +
                "\n" +
                "                        <div style=\"height: 70px;width: 200px;float: left; padding-top: 20px; padding-left: 50px;\">\n" +
                "                                <button style=\"float: left;\" class=\"reg1\">\n" +
                "                                        <a style=\"text-decoration: none; color: white;\" href=\"http://localhost:8080/CloudAddressBook/premium.html\">PREMIUM ACCOUNT</a>\n" +
                "                                </button>\n" +
                "                        </div>        \n" +
                "                        <div style=\"height: 70px;width: 200px;float: left; padding-top: 20px; padding-left:50px;\">        \n" +
                "                                <a href=\"http://localhost:8080/CloudAddressBook/sharing\"><button style=\"float: left;\" class=\"reg1\">\n" +
                "                                        SHARING\n" +
                "                                </button></a>\n" +
                "                        </div>                        \n" +
                "                        <div style=\"height: 70px;width: 200px;float: left; padding-top: 20px; padding-left: 50px;\">\n" +
                "                                <button style=\"float: left;\" class=\"reg1\">\n" +
                "                                        <a style=\"text-decoration: none; color: white;\" href=\"http://localhost:8080/CloudAddressBook/addnew.html\">ADD NEW PERSON </a>\n" +
                "                                </button>\n" +
                "                              \n" +
                "                        </div>        \n" +
                "                        <div style=\"height: 70px;width: 200px;float: left; padding-top: 20px; padding-left:50px;\">        \n" +
                "                                <button style=\"float: left;\" class=\"reg1\">\n" +
                "                                        <a style=\"text-decoration: none; color: white;\" href=\"http://localhost:8080/CloudAddressBook/setting.html\">MORE FEATURES</a>\n" +
                "                                </button>\n" +
                "                        </div>\n" +
                "                        <div style=\"height: 70px;width: 70px;float: left; padding-top: 20px; padding-left:50px;\">\n" +
                "                                <a href=\"http://localhost:8080/CloudAddressBook/close\">\n" +
                "                                \t<button style=\"float: left;font-size: 20px;\" class=\"\">X\n" +
                " \t\t\t\t\t\t</button></a>\n" +
                "                        </div>\n" +
                "                </div>\n" +
                "        </header>"+
                "        <article>\n" +
                "                \n" +
                "                <center>\n" +
                "                <div class=\"art\">\n" +
                "                        <div class=\"login\">\n" +
                "                                <form action=\"search\" method=\"POST\">\n" +
                "                                        <label style=\"font-size: 20px; color:white;\">Name</label>\n" +
                "                                        <input type=\"text\" name=\"name\" style=\"font-size: 20px;border-radius: 10px; margin-left: 17px;\"><br><br>\n" +
                "                                        <label style=\"font-size: 20px; color:white;\">Surname</label>\n" +
                "                                        <input class=\"p\" type=\"text\" name=\"surname\" style=\"font-size: 20px; border-radius: 10px;\"><br><br>\n" +
                "                                        <input type=\"submit\" class=\"reg1\" value=\"SEARCH\" name=\"\">\n" +
                "                                </form>\n" +"<form action=\"attitudes\" method=\"GET\">\n" +
                "                                \t<input type=\"submit\" class=\"reg1\" value=\"COLLEAGUES\" name=\"colleagues\">\n" +
                "                                \t<input type=\"submit\" class=\"reg1\" value=\"FRIENDS\" name=\"friends\">\n" +
                "                                \t<input type=\"submit\" class=\"reg1\" value=\"STUDENTS\" name=\"students\">\n" +
                "                                \t<input type=\"submit\" class=\"reg1\" value=\"RELATIVE\" name=\"relative\">\n" +
                "                                </form>"+
                "                        </div>\n" +
                "                        <div id =\"table\" style=\"height:700px;width: 800px; background-color: white; margin-top:20px;border:3px black solid;\">\n" +
                "                                <div style=\"float: left; padding-top: 10px; padding-left: 20px;\">\n" +
                "                                <label style=\"font-size: 20px; float: left; padding-left: 10px;\">Search results</label>\n" +
                "                                <br/>\n" +
                "                                <table style=\\\"text-align:left\\\">\n" +
                "                                <tr style=\"font-size: 17px;\">\n" +
                "                                        <th style=\"padding-left: 10px\">Name</th><th style=\"padding-left: 15px\">Surname</th><th style=\"padding-left: 15px\">Paternalname</th>\n" +
                "                                </tr>";
        for(Object obj : arrayList) {
            Person person = (Person) obj;
            int id = person.getId();
            String n = person.getName();
            String s = person.getSurname();
            String p = person.getPaternalname();
            str += "<tr style=\"font-size: 17px;\">" +
                    "<th style=\"padding-left: 10px\">"+n+"</th><th style=\"padding-left: 15px\">"+s+"</th><th style=\"padding-left: 15px\">"+p+"</th>"+"<th>"+"<a href=\"http://localhost:8080/CloudAddressBook/more?id="+id+"\"><button>MORE</button></a><a href=\"http://localhost:8080/CloudAddressBook/delete?id="+id+"\"><button style=\"margin-left: 5px;\">DEL</button></a>"+"\t<form style=\"padding-top: 5px;\" action=\"attitudes?id="+id+"\" method=\"POST\">\n" +
                    "                                \t\t<select name=\"param\">\n" +
                    "                                \t\t\t<option>ATTITUDES</option>\n" +
                    "                                \t\t\t<option>COLLEAGUES</option>\n" +
                    "                                \t\t\t<option>FRIENDS</option>\n" +
                    "                                \t\t\t<option>STUDENTS</option>\n" +
                    "                                \t\t\t<option>RELATIVE</option>\n" +
                    "                                \t\t</select>\n" +
                    "                                \t\t<input type=\"submit\" name=\"\" value=\"+\">\n" +
                    "                                \t</form></th>"+
                    "</tr>";
        }
        str+="</table>\n" +
                "                                </div>\n" +
                "                        </div>\n" +
                "                </div>\n" +
                "                </center>\n" +
                "        </article>\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        return str;
    }
}
