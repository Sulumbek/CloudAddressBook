import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SeqModule {

    static int currentSequence(String seq){
        int id = 0;
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        try{
            Connection connection = myConnect.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from "+seq+";");
            if(resultSet.next()) id = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        myConnect.closeConnect();
        return id;
    }
}
