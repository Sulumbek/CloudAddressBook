public class MailContact extends Contact{

    private String mail;

    MailContact(String value) {
        this.mail = value;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
