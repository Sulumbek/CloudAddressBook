import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

class SessionsUsers {

    private int userId;

    SessionsUsers(int userId) {
        this.userId = userId;
    }
    SessionsUsers(){}

    public void setUserId(int userId) {
        this.userId = userId;
    }

    int getUserId(){
        return userId;
    }

    static int getIdByEmail(String email) throws SQLException {
        MyConnect myConnect = new MyConnect();
        myConnect.openConnect();
        Connection connection = myConnect.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * from users where email='"+email+"' ");
        if (resultSet.next()) return resultSet.getInt("id");
        myConnect.closeConnect();
        return 0;
    }

    void addAttitudes(int personId,int userId ,String name) {
        try {
            AttitudesModule.setAttitudesInDataBase(personId,userId,name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void usersAcceptor(int id) {
        try {
            UserModule.usersAcceptorInDataBase(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    String getAttitudes(String param) {
        try {
            return CodeHtml.codeViewPersonsList(AttitudesModule.getAttitudesOfDataBase(userId,param));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    void deletePerson(int personId) {
        try {
            PersonsModule.deletePersonOfDataBase(personId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    void addPersons(Person person) {
        try {
            PersonsModule.addPersonsInDataBase(userId,person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void updateCountPersons(int countPerson){
        try {
            PersonsModule.updateCountPersonsInDataBase(countPerson,userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    boolean verification(String login, String password) {
        boolean bool = false;
        try {
            bool =  VerificationModule.verificationLoginAndEmail(login ,password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bool;
    }

    Person getPersonById(int id) {
        Person person = null;
        try {
            person = PersonsModule.getPersonsByIdOfDataBase(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    void addPremium(String name, int months, String key) {
        try {
            UserModule.addPremiumUserAccount(userId,name,months,key);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ArrayList<Person> findPersons(String qName, String qSurname) {
        ArrayList<Person>arrayList = null;
        try {
            arrayList = PersonsModule.searchPersons(userId,qName, qSurname);
        } catch (SQLException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    void addUsers(User user, Person person) {
        try {
            UserModule.addUsersInDataBase(user,person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    int getCurrentCountPersons() {
        try {
            return PersonsModule.currentCountPersons(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    User getUsersById() {
        try {
            return UserModule.getUsersById(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    Person getUserDataById(){
        try {
            return UserModule.getUserData(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    void editUser(String field, String key) {
        try {
            UserModule.edit(userId,field,key);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void editPerson(String field,String key) {
        try {
            PersonsModule.editPersonInDataBase(userId,field,key);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void editLocation(String country,String city) {
        try {
            PersonsModule.editLocationInDataBase(userId,country,city);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
